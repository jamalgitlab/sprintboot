package com.stack.resetservices;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorld {
	
	//@RequestMapping(method = RequestMethod.GET, path="/helloworld")
	@GetMapping("/helloworld")
	public String helloworld() {
		return "Hello World new";
	}
	
	@GetMapping("/getuserinfo")
	public UserDetails getuserinfo() {
		return new UserDetails("Murali Krishnan", "Sankaran", "Madurai", 35);
	}

}
