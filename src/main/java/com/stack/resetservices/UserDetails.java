package com.stack.resetservices;

public class UserDetails {
	
	private String firstName;
	private String lastName;
	private String city;
	private int age;
	
	
	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public UserDetails(String firstName, String lastName, String city, int age) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.city = city;
		this.age= age;
	}
	
	//No use of this method adsfdsf
	@Override
	public String toString() {
		return "UserDetails [firstName=" + firstName + ", lastName=" + lastName + ", city=" + city + "]";
	}

	public String getFirstName() {
		return firstName;
	}
	
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	
	public String getLastName() {
		return lastName;
	}
	
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getCity() {
		return city;
	}
	
	public void setCity(String city) {
		this.city = city;
	}
	

}
