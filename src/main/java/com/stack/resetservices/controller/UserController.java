package com.stack.resetservices.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;

import com.stack.resetservices.entities.User;
import com.stack.resetservices.exception.UserCreationException;
import com.stack.resetservices.exception.UserNotFoundException;
import com.stack.resetservices.services.UserService;

@RestController
public class UserController {
	
	@Autowired
	private UserService userService;
	
	@GetMapping("/users")
	public List<User> getAllUser(){
		return userService.getAllUser();
	}
	
	@PostMapping("/createuser")
	public User createUser(@RequestBody User user, UriComponentsBuilder builder) {		
		try {
			return userService.createUser(user);
			//HttpHeaders headers = new HttpHeaders();
			//headers.setLocation(builder.path("/createuser"));
			
		} catch (UserCreationException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}		
	}
	
	@GetMapping("/users/{id}")
	public Optional<User> getUserById(@PathVariable("id") Long id){
		try {
			return userService.getUserById(id);
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
	
	@PutMapping("/users/{id}")
	public User updateUserById(@PathVariable("id") Long id, @RequestBody User user) {
		try {
			return userService.updateUserById(id, user);
		} catch (UserNotFoundException e) {
			// TODO Auto-generated catch block
			throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
		}
	}
	
	@DeleteMapping("/deleteusers/{id}")
	public String deleteUserById(@PathVariable("id") Long id) {
		return userService.deleteUserById(id);
	}
	
	@GetMapping("/users/userbyname/{username}")
	public User getUserByUserName(@PathVariable("username") String username){
		return userService.getUserByUserName(username);
	}
	

}
